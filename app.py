from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template, url_for
from werkzeug.datastructures import WWWAuthenticate
import flask
import flask_login
from flask_login import login_required, login_user
from login_form import LoginForm
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
import hashlib
import sqlite3
import json

tls = local()
conn = None
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
loginconn = sqlite3.connect("login.db")
loginc=loginconn.cursor()

application = Flask(__name__)
application.secret_key = "AidASIb7ASDB02ASIuhbas8283SADixxidkhowlongishouldmakethis"

login_manager = flask_login.LoginManager()
login_manager.init_app(application)
login_manager.login_view = "login"

loginc.execute("SELECT * from logins")
dbfetch = loginc.fetchall()
users = {}
for x in dbfetch:
    innerdict = {'password' : x[1]}
    users[x[0]]=innerdict

class User(flask_login.UserMixin):
    pass

@login_manager.user_loader
def user_loader(user_id):
    if user_id not in users:
        return

    user = User()
    user.id = user_id
    return user

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@application.route('/')
@application.route('/index.html')
@login_required
def index_html():
    return render_template('./index.html', user = username)


@application.route('/login', methods=['GET', 'POST'])
def login():
    global username
    form = LoginForm()
    print(form)
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
    if form.validate_on_submit():
        username = form.username.data
        password = hashlib.sha256(form.password.data.encode("utf-8")).hexdigest()
        if username in users.keys() and password == users.get(username).get("password"):
            print("password good")
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)
            return flask.redirect(url_for("index_html", user = username)) 
        else:
            print("password wrongg")
    return render_template('./login.html', form=form)

@application.route('/register', methods=["GET", 'POST'])
def register():
    print("entered reg")
    loginconn = sqlite3.connect("login.db")
    username = request.args.get('username')
    password = request.args.get('password')
    loginc=loginconn.cursor()
    userc = loginc.execute("SELECT username FROM logins")
    for user in userc:
        if user[0].lower()==username.lower():
            return "username is taken"
    passwordhashed = hashlib.sha256(password.encode("utf-8")).hexdigest()
    try:
        loginc.execute("INSERT INTO logins VALUES (?, ?)", (username, passwordhashed))
    except:
        return "something went wrong"
    loginconn.commit()
    users[username]={'password' : passwordhashed}
    return "you have been registered, please log in"

@application.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

@application.get('/search')
def search():
    query = request.args.get('q') or request.form.get('q') or '*'
    user = request.args.get('u')
    result = []
    try:
        c = conn.execute("SELECT * FROM messages WHERE message GLOB ? and (recipient = ? or sender = ?)", (query, user, user))
        rows = c.fetchall()
        for row in rows:
            result.append(row)
        c.close()
        return json.dumps(result)
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@application.route('/send', methods=['POST','GET'])
def send():
    try:
        sender = request.args.get('sender') or request.form.get('sender')
        message = request.args.get('message') or request.args.get('message')
        recipient = request.args.get('recipient')
        if not sender or not message or not recipient:
            return f'ERROR: missing sender, recipient or message'
        stmt = f"To: {recipient}\n {message}\n From: {sender}"
        result = f"{stmt}\n"
        conn.execute("INSERT INTO messages (sender, message, recipient) values (?, ?, ?);", (sender, message, recipient))
        return f'{result}'
    except Error as e:
        return f'{result}ERROR: {e}'

try:
    conn = apsw.Connection('./content.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        message TEXT NOT NULL,
        recipient TEXT NOT NULL);''')
except Error as e:
    print(e)
    sys.exit(1)